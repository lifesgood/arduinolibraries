/****************************************************************************/	
//	Function: Set time and get the time from RTC chip(DS1307) and display 
//			  it on the serial monitor.
//	Hardware: Grove - RTC
//	Arduino IDE: Arduino-1.0
//	Author:	 FrankieChu		
//	Date: 	 Jan 19,2013
//	Version: v1.0
//	by www.seeedstudio.com
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public
//  License as published by the Free Software Foundation; either
//  version 2.1 of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
/****************************************************************************/
#include <Wire.h>
#include "DS1307.h"
#include <TM1637Display.h>

// Module connection pins (Digital Pins)
#define CLK 2
#define DIO 3

TM1637Display display(CLK, DIO);//define a object of TM1637 class

DS1307 clock;//define a object of DS1307 class


void setup()
{
	
  display.setBrightness(7);

	//Serial.begin(9600);
	clock.begin();
	//clock.fillByYMD(2013,1,19);//Jan 19,2013
	//clock.fillByHMS(15,28,30);//15:28 30"
	//clock.fillDayOfWeek(SAT);//Saturday
	//clock.setTime();//write time to the RTC chip
}
void loop()
{
	printTime();
}
/*Function: Display time on the serial monitor*/
void printTime()
{
  uint8_t data[] = { 0xff, 0xff, 0xff, 0xff };
	clock.getTime();
  data[0] = display.encodeDigit((clock.hour,DEC  )/10);
  data[1] = display.encodeDigit((clock.hour,DEC  )%10);
  data[2] = display.encodeDigit((clock.minute,DEC)/10);
  data[3] = display.encodeDigit((clock.minute,DEC)%10);
  bool DOUBLE_POINT = 1;
  if (((clock.second)%2)>>0) 
    DOUBLE_POINT = 0;
 
  display.setSegments(data,DOUBLE_POINT);
	
}
